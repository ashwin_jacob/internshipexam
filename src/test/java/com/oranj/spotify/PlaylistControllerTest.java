package com.oranj.spotify;

import org.junit.Before;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PlaylistControllerTest {
    private PlaylistService playlistService;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        playlistService = mock(PlaylistService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(new PlaylistController(playlistService)).build();
    }

    @Test
    public void grabAlbum_returns200_withAlbumName() throws Exception {
        String spotifyId = "4m2880jivSbbyEGAKfITCa";
        AlbumDTO albumDTO = new AlbumDTO();
        albumDTO.setAlbum("Random Access Memories");
        List<String> listOfArtists = new ArrayList<>();
        listOfArtists.add("Daft Punk");
        albumDTO.setArtist(listOfArtists);

        when(playlistService.grabAlbumInformation(spotifyId)).thenReturn(albumDTO);
        String contentAsString = mockMvc.perform(get("/album?spotifyId="+spotifyId)).andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        JSONAssert.assertEquals("{'artist': ['Daft Punk'], 'album': 'Random Access Memories'}", contentAsString, JSONCompareMode.LENIENT);
    }


}
