package com.oranj.spotify;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class PlaylistServiceTest {
    private PlaylistService playlistService;
    private MockRestServiceServer mockRestServer;

    @Before
    public void setup() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        mockRestServer = MockRestServiceServer.createServer(restTemplateBuilder.build());

        playlistService = new PlaylistService(restTemplateBuilder);
    }

    @Test
    public void getAlbumInformation_returnAlbumDTO() {
        AlbumDTO expectedAlbumDTO = new AlbumDTO();
        expectedAlbumDTO.setAlbum("Random Access Memories");
        List<String> listOfArtists = new ArrayList<>();
        listOfArtists.add("Daft Punk");
        expectedAlbumDTO.setArtist(listOfArtists);

        String spotifyId = "4m2880jivSbbyEGAKfITCa";
        mockRestServer.expect(MockRestRequestMatchers.requestTo("https://api.spotify.com/v1/albums/4m2880jivSbbyEGAKfITCa"))
                .andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
                .andRespond(MockRestResponseCreators.withSuccess("{\n" +
                        "  \"album_type\": \"album\",\n" +
                        "  \"artists\": [\n" +
                        "    {\n" +
                        "      \"external_urls\": {\n" +
                        "        \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "      },\n" +
                        "      \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "      \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "      \"name\": \"Daft Punk\",\n" +
                        "      \"type\": \"artist\",\n" +
                        "      \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"available_markets\": [\n" +
                        "    \"AD\",\n" +
                        "    \"AR\",\n" +
                        "    \"AT\",\n" +
                        "    \"AU\",\n" +
                        "    \"BE\",\n" +
                        "    \"BG\",\n" +
                        "    \"BO\",\n" +
                        "    \"BR\",\n" +
                        "    \"CA\",\n" +
                        "    \"CH\",\n" +
                        "    \"CL\",\n" +
                        "    \"CO\",\n" +
                        "    \"CR\",\n" +
                        "    \"CY\",\n" +
                        "    \"CZ\",\n" +
                        "    \"DE\",\n" +
                        "    \"DK\",\n" +
                        "    \"DO\",\n" +
                        "    \"EC\",\n" +
                        "    \"EE\",\n" +
                        "    \"ES\",\n" +
                        "    \"FI\",\n" +
                        "    \"FR\",\n" +
                        "    \"GB\",\n" +
                        "    \"GR\",\n" +
                        "    \"GT\",\n" +
                        "    \"HK\",\n" +
                        "    \"HN\",\n" +
                        "    \"HU\",\n" +
                        "    \"ID\",\n" +
                        "    \"IE\",\n" +
                        "    \"IS\",\n" +
                        "    \"IT\",\n" +
                        "    \"JP\",\n" +
                        "    \"LI\",\n" +
                        "    \"LT\",\n" +
                        "    \"LU\",\n" +
                        "    \"LV\",\n" +
                        "    \"MC\",\n" +
                        "    \"MT\",\n" +
                        "    \"MX\",\n" +
                        "    \"MY\",\n" +
                        "    \"NI\",\n" +
                        "    \"NL\",\n" +
                        "    \"NO\",\n" +
                        "    \"NZ\",\n" +
                        "    \"PA\",\n" +
                        "    \"PE\",\n" +
                        "    \"PH\",\n" +
                        "    \"PL\",\n" +
                        "    \"PT\",\n" +
                        "    \"PY\",\n" +
                        "    \"SE\",\n" +
                        "    \"SG\",\n" +
                        "    \"SK\",\n" +
                        "    \"SV\",\n" +
                        "    \"TR\",\n" +
                        "    \"TW\",\n" +
                        "    \"US\",\n" +
                        "    \"UY\"\n" +
                        "  ],\n" +
                        "  \"copyrights\": [\n" +
                        "    {\n" +
                        "      \"text\": \"(P) 2013 Daft Life Limited under exclusive license to Columbia Records, a Division of Sony Music Entertainment\",\n" +
                        "      \"type\": \"P\"\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"external_ids\": {\n" +
                        "    \"upc\": \"886443927087\"\n" +
                        "  },\n" +
                        "  \"external_urls\": {\n" +
                        "    \"spotify\": \"https://open.spotify.com/album/4m2880jivSbbyEGAKfITCa\"\n" +
                        "  },\n" +
                        "  \"genres\": [],\n" +
                        "  \"href\": \"https://api.spotify.com/v1/albums/4m2880jivSbbyEGAKfITCa\",\n" +
                        "  \"id\": \"4m2880jivSbbyEGAKfITCa\",\n" +
                        "  \"images\": [\n" +
                        "    {\n" +
                        "      \"height\": 636,\n" +
                        "      \"url\": \"https://i.scdn.co/image/720c461b76e099758677df449324572e124c8fb4\",\n" +
                        "      \"width\": 640\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"height\": 298,\n" +
                        "      \"url\": \"https://i.scdn.co/image/405ee050d1976448c600cb9648e491e31ef87aed\",\n" +
                        "      \"width\": 300\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"height\": 64,\n" +
                        "      \"url\": \"https://i.scdn.co/image/aa9dbc54193b3c0c2f7fd9cc9a914b1c09bf058c\",\n" +
                        "      \"width\": 64\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"label\": \"Columbia\",\n" +
                        "  \"name\": \"Random Access Memories\",\n" +
                        "  \"popularity\": 72,\n" +
                        "  \"release_date\": \"2013-05-17\",\n" +
                        "  \"release_date_precision\": \"day\",\n" +
                        "  \"tracks\": {\n" +
                        "    \"href\": \"https://api.spotify.com/v1/albums/4m2880jivSbbyEGAKfITCa/tracks?offset=0&limit=50\",\n" +
                        "    \"items\": [\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 275386,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/0dEIca2nhcxDUV8C5QkPYb\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/0dEIca2nhcxDUV8C5QkPYb\",\n" +
                        "        \"id\": \"0dEIca2nhcxDUV8C5QkPYb\",\n" +
                        "        \"name\": \"Give Life Back to Music\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/29c9a5d90d26a12c9dd360aabc1d23b1d6435242?cid=null\",\n" +
                        "        \"track_number\": 1,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:0dEIca2nhcxDUV8C5QkPYb\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 322146,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/3ctALmweZBapfBdFiIVpji\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/3ctALmweZBapfBdFiIVpji\",\n" +
                        "        \"id\": \"3ctALmweZBapfBdFiIVpji\",\n" +
                        "        \"name\": \"The Game of Love\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/7db68bcbbd991befe3aa4eb06d156e26d26a49ed?cid=null\",\n" +
                        "        \"track_number\": 2,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:3ctALmweZBapfBdFiIVpji\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 544626,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/0oks4FnzhNp5QPTZtoet7c\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/0oks4FnzhNp5QPTZtoet7c\",\n" +
                        "        \"id\": \"0oks4FnzhNp5QPTZtoet7c\",\n" +
                        "        \"name\": \"Giorgio by Moroder\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/534cabcdaafed760cb64f02c45eee6ca5e8e9b3e?cid=null\",\n" +
                        "        \"track_number\": 3,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:0oks4FnzhNp5QPTZtoet7c\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 228506,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/7Bxv0WL7UC6WwQpk9TzdMJ\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/7Bxv0WL7UC6WwQpk9TzdMJ\",\n" +
                        "        \"id\": \"7Bxv0WL7UC6WwQpk9TzdMJ\",\n" +
                        "        \"name\": \"Within\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/5e6d91adada11bca669d801169952267604b9097?cid=null\",\n" +
                        "        \"track_number\": 4,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:7Bxv0WL7UC6WwQpk9TzdMJ\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/1rAv1GhTQ2rmG94p9lU3rB\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/1rAv1GhTQ2rmG94p9lU3rB\",\n" +
                        "            \"id\": \"1rAv1GhTQ2rmG94p9lU3rB\",\n" +
                        "            \"name\": \"Julian Casablancas\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:1rAv1GhTQ2rmG94p9lU3rB\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 337560,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/2cGxRwrMyEAp8dEbuZaVv6\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/2cGxRwrMyEAp8dEbuZaVv6\",\n" +
                        "        \"id\": \"2cGxRwrMyEAp8dEbuZaVv6\",\n" +
                        "        \"name\": \"Instant Crush\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/2dc001a8d4bb5666dfb8b5d544b73d3ec0e3a92a?cid=null\",\n" +
                        "        \"track_number\": 5,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:2cGxRwrMyEAp8dEbuZaVv6\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/2RdwBSPQiwcmiDo9kixcl8\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/2RdwBSPQiwcmiDo9kixcl8\",\n" +
                        "            \"id\": \"2RdwBSPQiwcmiDo9kixcl8\",\n" +
                        "            \"name\": \"Pharrell Williams\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:2RdwBSPQiwcmiDo9kixcl8\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 353893,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/5CMjjywI0eZMixPeqNd75R\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/5CMjjywI0eZMixPeqNd75R\",\n" +
                        "        \"id\": \"5CMjjywI0eZMixPeqNd75R\",\n" +
                        "        \"name\": \"Lose Yourself to Dance\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/24e8d6ba5f91d46782dc44ca9eb44db18b1fc897?cid=null\",\n" +
                        "        \"track_number\": 6,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:5CMjjywI0eZMixPeqNd75R\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/3f626JSVauIhTQgatsFcs4\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/3f626JSVauIhTQgatsFcs4\",\n" +
                        "            \"id\": \"3f626JSVauIhTQgatsFcs4\",\n" +
                        "            \"name\": \"Paul Williams\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:3f626JSVauIhTQgatsFcs4\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 498960,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/7oaEjLP2dTJLJsITbAxTOz\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/7oaEjLP2dTJLJsITbAxTOz\",\n" +
                        "        \"id\": \"7oaEjLP2dTJLJsITbAxTOz\",\n" +
                        "        \"name\": \"Touch\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/1469e37ce94e41d93a6e3777d313d5cb172d4dbc?cid=null\",\n" +
                        "        \"track_number\": 7,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:7oaEjLP2dTJLJsITbAxTOz\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/2RdwBSPQiwcmiDo9kixcl8\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/2RdwBSPQiwcmiDo9kixcl8\",\n" +
                        "            \"id\": \"2RdwBSPQiwcmiDo9kixcl8\",\n" +
                        "            \"name\": \"Pharrell Williams\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:2RdwBSPQiwcmiDo9kixcl8\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/3yDIp0kaq9EFKe07X1X2rz\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/3yDIp0kaq9EFKe07X1X2rz\",\n" +
                        "            \"id\": \"3yDIp0kaq9EFKe07X1X2rz\",\n" +
                        "            \"name\": \"Nile Rodgers\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:3yDIp0kaq9EFKe07X1X2rz\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 369626,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/69kOkLUCkxIZYexIgSG8rq\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/69kOkLUCkxIZYexIgSG8rq\",\n" +
                        "        \"id\": \"69kOkLUCkxIZYexIgSG8rq\",\n" +
                        "        \"name\": \"Get Lucky\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/28c1195dcc64c65223ed1f439886d50f4ded7ba6?cid=null\",\n" +
                        "        \"track_number\": 8,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:69kOkLUCkxIZYexIgSG8rq\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 290240,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/0k1xMUwn9sb7bZiqdT9ygx\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/0k1xMUwn9sb7bZiqdT9ygx\",\n" +
                        "        \"id\": \"0k1xMUwn9sb7bZiqdT9ygx\",\n" +
                        "        \"name\": \"Beyond\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/cbea5e9ee5d06392321d3536f3e5af57d82e5d8b?cid=null\",\n" +
                        "        \"track_number\": 9,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:0k1xMUwn9sb7bZiqdT9ygx\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 341653,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/79koEJRtKOOGJ0VSAF3FMk\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/79koEJRtKOOGJ0VSAF3FMk\",\n" +
                        "        \"id\": \"79koEJRtKOOGJ0VSAF3FMk\",\n" +
                        "        \"name\": \"Motherboard\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/ef0433b191bf290a8f0d236f94713297745f129a?cid=null\",\n" +
                        "        \"track_number\": 10,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:79koEJRtKOOGJ0VSAF3FMk\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/6MFopqejpmTUUZlcRmGzgg\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/6MFopqejpmTUUZlcRmGzgg\",\n" +
                        "            \"id\": \"6MFopqejpmTUUZlcRmGzgg\",\n" +
                        "            \"name\": \"Todd Edwards\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:6MFopqejpmTUUZlcRmGzgg\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 279773,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/0IedgQjjJ8Ad4B3UDQ5Lyn\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/0IedgQjjJ8Ad4B3UDQ5Lyn\",\n" +
                        "        \"id\": \"0IedgQjjJ8Ad4B3UDQ5Lyn\",\n" +
                        "        \"name\": \"Fragments of Time\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/ad92e2df9ff31966724938c6b0021914eb53d486?cid=null\",\n" +
                        "        \"track_number\": 11,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:0IedgQjjJ8Ad4B3UDQ5Lyn\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          },\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/1R84VlXnFFULOsWWV8IrCQ\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/1R84VlXnFFULOsWWV8IrCQ\",\n" +
                        "            \"id\": \"1R84VlXnFFULOsWWV8IrCQ\",\n" +
                        "            \"name\": \"Panda Bear\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:1R84VlXnFFULOsWWV8IrCQ\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 251293,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/36c4JohayB9qd64eidQMBi\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/36c4JohayB9qd64eidQMBi\",\n" +
                        "        \"id\": \"36c4JohayB9qd64eidQMBi\",\n" +
                        "        \"name\": \"Doin' it Right\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/009b72b43f6462c954d37501fe54439f37f2ef59?cid=null\",\n" +
                        "        \"track_number\": 12,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:36c4JohayB9qd64eidQMBi\"\n" +
                        "      },\n" +
                        "      {\n" +
                        "        \"artists\": [\n" +
                        "          {\n" +
                        "            \"external_urls\": {\n" +
                        "              \"spotify\": \"https://open.spotify.com/artist/4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "            },\n" +
                        "            \"href\": \"https://api.spotify.com/v1/artists/4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"id\": \"4tZwfgrHOc3mvqYlEYSvVi\",\n" +
                        "            \"name\": \"Daft Punk\",\n" +
                        "            \"type\": \"artist\",\n" +
                        "            \"uri\": \"spotify:artist:4tZwfgrHOc3mvqYlEYSvVi\"\n" +
                        "          }\n" +
                        "        ],\n" +
                        "        \"available_markets\": [\n" +
                        "          \"AD\",\n" +
                        "          \"AR\",\n" +
                        "          \"AT\",\n" +
                        "          \"AU\",\n" +
                        "          \"BE\",\n" +
                        "          \"BG\",\n" +
                        "          \"BO\",\n" +
                        "          \"BR\",\n" +
                        "          \"CA\",\n" +
                        "          \"CH\",\n" +
                        "          \"CL\",\n" +
                        "          \"CO\",\n" +
                        "          \"CR\",\n" +
                        "          \"CY\",\n" +
                        "          \"CZ\",\n" +
                        "          \"DE\",\n" +
                        "          \"DK\",\n" +
                        "          \"DO\",\n" +
                        "          \"EC\",\n" +
                        "          \"EE\",\n" +
                        "          \"ES\",\n" +
                        "          \"FI\",\n" +
                        "          \"FR\",\n" +
                        "          \"GB\",\n" +
                        "          \"GR\",\n" +
                        "          \"GT\",\n" +
                        "          \"HK\",\n" +
                        "          \"HN\",\n" +
                        "          \"HU\",\n" +
                        "          \"ID\",\n" +
                        "          \"IE\",\n" +
                        "          \"IS\",\n" +
                        "          \"IT\",\n" +
                        "          \"JP\",\n" +
                        "          \"LI\",\n" +
                        "          \"LT\",\n" +
                        "          \"LU\",\n" +
                        "          \"LV\",\n" +
                        "          \"MC\",\n" +
                        "          \"MT\",\n" +
                        "          \"MX\",\n" +
                        "          \"MY\",\n" +
                        "          \"NI\",\n" +
                        "          \"NL\",\n" +
                        "          \"NO\",\n" +
                        "          \"NZ\",\n" +
                        "          \"PA\",\n" +
                        "          \"PE\",\n" +
                        "          \"PH\",\n" +
                        "          \"PL\",\n" +
                        "          \"PT\",\n" +
                        "          \"PY\",\n" +
                        "          \"SE\",\n" +
                        "          \"SG\",\n" +
                        "          \"SK\",\n" +
                        "          \"SV\",\n" +
                        "          \"TR\",\n" +
                        "          \"TW\",\n" +
                        "          \"US\",\n" +
                        "          \"UY\"\n" +
                        "        ],\n" +
                        "        \"disc_number\": 1,\n" +
                        "        \"duration_ms\": 383626,\n" +
                        "        \"explicit\": false,\n" +
                        "        \"external_urls\": {\n" +
                        "          \"spotify\": \"https://open.spotify.com/track/2KHRENHQzTIQ001nlP9Gdc\"\n" +
                        "        },\n" +
                        "        \"href\": \"https://api.spotify.com/v1/tracks/2KHRENHQzTIQ001nlP9Gdc\",\n" +
                        "        \"id\": \"2KHRENHQzTIQ001nlP9Gdc\",\n" +
                        "        \"name\": \"Contact\",\n" +
                        "        \"preview_url\": \"https://p.scdn.co/mp3-preview/c081a8ebb90dc43a114e505b012b4e68c88e7f3b?cid=null\",\n" +
                        "        \"track_number\": 13,\n" +
                        "        \"type\": \"track\",\n" +
                        "        \"uri\": \"spotify:track:2KHRENHQzTIQ001nlP9Gdc\"\n" +
                        "      }\n" +
                        "    ],\n" +
                        "    \"limit\": 50,\n" +
                        "    \"next\": null,\n" +
                        "    \"offset\": 0,\n" +
                        "    \"previous\": null,\n" +
                        "    \"total\": 13\n" +
                        "  },\n" +
                        "  \"type\": \"album\",\n" +
                        "  \"uri\": \"spotify:album:4m2880jivSbbyEGAKfITCa\"\n" +
                        "}", MediaType.APPLICATION_JSON));
        AlbumDTO actualAlbumDTO = playlistService.grabAlbumInformation(spotifyId);
        assertThat(actualAlbumDTO.getAlbum()).isEqualTo("Random Access Memories");
        assertThat(actualAlbumDTO.getArtist()).containsAll(listOfArtists);
    }
}
