package com.oranj.spotify;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.*;

@RestController
public class PlaylistController {
    private PlaylistService playlistService;

    @Autowired
    public PlaylistController(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

    @RequestMapping(value = "/album", method = RequestMethod.GET)
    AlbumDTO getAlbumInformation(@RequestParam("spotifyId") String spotifyID) {
        return playlistService.grabAlbumInformation(spotifyID);
    }
}
