package com.oranj.spotify;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * This is used for the spotify json data manipulation.
 * You need to give the Marshaller a class to store the data from Spotify.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Artist {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
