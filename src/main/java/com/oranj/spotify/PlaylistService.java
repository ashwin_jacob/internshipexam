package com.oranj.spotify;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlaylistService {
    private RestTemplate restTemplate;

    @Autowired
    public PlaylistService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public AlbumDTO grabAlbumInformation(String spotifyId) {
        Album spotfiyResponse = restTemplate.getForObject("https://api.spotify.com/v1/albums/"+spotifyId, Album.class);
        AlbumDTO returnResponse = new AlbumDTO();
        List<Artist> artists = spotfiyResponse.getArtists();
        List<String> artistList = new ArrayList<>();
        artists.stream()
                .forEach(artist -> {
                    artistList.add(artist.getName());
                });
        returnResponse.setArtist(artistList);
        returnResponse.setAlbum(spotfiyResponse.getName());
        return returnResponse;
    }
}
