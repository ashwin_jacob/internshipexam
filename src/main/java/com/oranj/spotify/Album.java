package com.oranj.spotify;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * This is used for the spotify json data manipulation.
 * You need to give the Marshaller a class to store the data from Spotify.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Album {
    private List<Artist> artists;
    private String name;

    public List<Artist> getArtists() {
        return artists;
    }

    public void setArtists(List<Artist> artists) {
        this.artists = artists;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
