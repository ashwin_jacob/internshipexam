package com.oranj.spotify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * This class is used to start a tomcat container to hit the endpoint on port 8080.
 * After you run this container, you can go to http://localhost:8080/album to get back album information.
 */
@SpringBootApplication
public class ApplicationInitializer extends SpringBootServletInitializer {
    public static void main(String [] args) {
        SpringApplication.run(ApplicationInitializer.class, args);
    }
}
