# Oranj Internship Exam

Welcome to the Oranj Internship Exam. You have been given the honor of taking this exam to see if you can be part of the awesome Oranj Team.

![image](https://lh3.googleusercontent.com/ah3rknU9rK-wZFuzFCq0Mfh5Do8ZMc1trl8A6uA4Tk94aLH98VNaN-ejrkShDXPsiFISIDMEuMztFNaxblGuLqbqSUMJqBDhIxfUEnrJO0af7B-GKms7cVoUdtp_5BlQi0tYX3I=w1071-h803-no)

## Requirements

* Java 8
* Maven (https://maven.apache.org/install.html)
* Postman

## Project
In order to run this project, just run the `ApplicationInitializer`. This will allow you to run the application and hit the given endpoint. There is only one endpoint for this application:

* GET - /album
	* Parameter: SpotifyID --- Unique ID from spotify to get the **album** information
		* Reference: https://developer.spotify.com/web-api/get-album/
	* Description: This will grab the album information from spotify
	
## Task
**Story**: Your job is to now save a User's Track Information to the Oranj Playlist DB. 

Acceptance Criteria:

* Create a new REST endpoint called `/playlist` that passses a spotify ID for a **track**
	* Reference: `http://developer.spotify.com/web-api/get-track/`
* Send a POST Request to the Oranj Playlist DB populating the DB with the user's track information
	* Required Values:
		* *playlistId*: Figure out a way to generate a unique playlist ID
		* *album*: Album Name
		* *artist*: Artist Name
		* *song*: Song Name
		* *url*: Fully Qualified URL from spotify
* Do a GET Request on the Oranj Playlist DB to grab the current playlist entries
* Send that information back to the end user

### Endpoint Collection
`https://www.getpostman.com/collections/fc97121fcf4d11fb6293`

If you go to this link, you should be able to retrieve a collection of endpoints that you need to finish this project. Please use Postman to import all the URLs.

## Sample Spotify IDs
### Album IDs
Album ID - Album, Artist

* 1YwzJz7CrV9fd9Qeb6oo1d --- Watch the Throne by JAY Z & Kanye West
* 1lXY618HWkwYKJWBRYR4MK --- More Life by Drake
* 3cQO7jp5S9qLBoIVtbkSM1 --- Blurryface by Twenty One Pilots
* 6Pb3K1oPXdhsqFXtzKe3Z1 --- 19 by Adele
* 4m2880jivSbbyEGAKfITCa --- Random Access Memories by Daft Punk

### Track IDs
Track ID - Track, Artist, Album

* 0aFObVR01JzPbBMjze4xPV --- No Church in the Wild, Jay Z & Kanye West, Watch the Throne
* 05KOgYg8PGeJyyWBPi5ja8 --- Free Smoke, Drake, More Life
* 3CRDbSIZ4r5MsZ0YwxuEkn --- Stressed Out, Twenty One Pilots, Blurryface
* 2MUH1tZ71fU7JPcZLxXT6t --- Hometown Glory, Adele, 19
* 0dEIca2nhcxDUV8C5QkPYb --- Give Life Back To Music, Daft Punk, Random Access Memories